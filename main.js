/*
 _______________________________________________
/ \                                             \
\_| FFG STAR WARS STATUS EFFECTS                |
  |                                             |
  | - new status effects                        |
  | - larger effect rendering                   |
  |                                             |
  |  ___________________________________________|_
  \_/____________________________________________/
*/



/* -------------------------------------------- */
/*  HOOKS                                       */
/* -------------------------------------------- */

// Run at startup.
Hooks.on("init", function() {
    console.log("FFG Star Wars Token Markers loading.");
    Init();
});

/* -------------------------------------------- */



// Overwrite the original status effects with our own.
CONFIG.statusEffects = [
	"modules/ffg-star-wars-status-effects/icons/plus_purple.svg",
	"modules/ffg-star-wars-status-effects/icons/plus_blue.svg",
	"modules/ffg-star-wars-status-effects/icons/plus_black.svg",
	"modules/ffg-star-wars-status-effects/icons/target_blue.svg",
	"modules/ffg-star-wars-status-effects/icons/target_black.svg",
	"modules/ffg-star-wars-status-effects/icons/upgrade_purple.svg",
	"modules/ffg-star-wars-status-effects/icons/upgrade_target_purple.svg",
	"modules/ffg-star-wars-status-effects/icons/upgrade_green.svg",
	"modules/ffg-star-wars-status-effects/icons/upgrade_target_green.svg"
];    



/* -------------------------------------------- */
/*  FUNCTIONS                                   */
/* -------------------------------------------- */

// patch Token.drawEffects
function Init() {
	Token.prototype.drawEffects = _patchDrawEffects;	
}



// Pretty much the original Foundry function.
// Added/increased mult and div for larger effects.
async function _patchDrawEffects() {
//	let mult = 2;
//	let divi = 5;

	let mult = 4;
	let divi = 10;


	this.effects.removeChildren().forEach(c => c.destroy());

    // Draw status effects
    if (this.data.effects.length > 0 ) {

      // Determine the grid sizing for each effect icon
      let w = Math.round(canvas.dimensions.size / 2 / 5) * mult;

      // Draw a background Graphics object
      let bg = this.effects.addChild(new PIXI.Graphics()).beginFill(0x000000, 0.40).lineStyle(1.0, 0x000000);

      // Draw each effect icon
      for ( let [i, src] of this.data.effects.entries() ) {
        let tex = await loadTexture(src);
        let icon = this.effects.addChild(new PIXI.Sprite(tex));
        icon.width = icon.height = w;
        icon.x = Math.floor(i / divi) * w;
        icon.y = (i % divi) * w;
        bg.drawRoundedRect(icon.x + 1, icon.y + 1, w - 2, w - 2, 2);
        this.effects.addChild(icon);
      }
    }

    // Draw overlay effect
    if ( this.data.overlayEffect ) {
      let tex = await loadTexture(this.data.overlayEffect);
      let icon = new PIXI.Sprite(tex),
          size = Math.min(this.w * 0.6, this.h * 0.6);
      icon.width = icon.height = size;
      icon.position.set((this.w - size) / 2, (this.h - size) / 2);
      icon.alpha = 0.80;
      this.effects.addChild(icon);
    }
}
